build:
	docker build -f Dockerfile.isfrekvens . -t image_isfrekvens  

interactive:
	docker run  -it -v /mnt/icecharts/NSIDC/SIC/v3/daily:/data -t image_isfrekvens /bin/bash

run:
	docker run -v /mnt/icecharts/NSIDC/SIC/v3/daily:/data -t image_isfrekvens
