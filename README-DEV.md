This project is supposed to be run as follows:

First obtain the data. The individual daily sea ice concentration files are put into the same folder.

Second, pull the docker image.

Third, execute docker image as an app, with storage volume mounted. 
