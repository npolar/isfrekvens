# Sea Ice Frequency Dataset

Sea Ice Frequency charts display the percentage of ice-covered days of a given month over the last 30 years. These charts, available as GeoTIFF raster data, are derived from daily sea ice concentration data provided by the National Snow and Ice Data Center (NSIDC), USA. Ice frequency is defined as the frequency of occurence of sea ice concentration values equal or higher than 0.15.  A value of 100% indicates areas where every single day of a given month sea ice concentrations were equal or higher than 0.15, while a value of 0% indicates areas where such sea ice concentrations have never been observed. A value of 0.5% indicates that sea ice concentrations were equal or higher than 0.15 during approximately 45 days within 30 years period for that particular month.

In addition to the raster files in GeoTIFF format vector files in ESRI Shapefile format are provided. They indicate the values of ice frequency in 10 percent increments. To reduce the noise and improve visual representation of the data we set the lower and upper threshold to 0.5% and 99.5% respectively. Please refer to the raster files for exact pixel values.

## Reserved values
Several mask values are reserved in the raster dataset:
* -1 - Missing data
* -2 - Landmask
* -3 - Coastal areas

## Coordinate reference system and extent
The dataset comes in the same geographic projection as the source NSIDC dataset: EPSG 3411 or "NSIDC Polar Stereographic North". Please refer to https://nsidc.org/data/polar-stereo/ps_grids.html for the complete grid definition.

## References
* Meier, W., F. Fetterer, M. Savoie, S. Mallory, R. Duerr, and J. Stroeve. 2017. NOAA/NSIDC Climate Data Record of Passive Microwave Sea Ice Concentration, Version 3. Boulder, Colorado USA. NSIDC: National Snow and Ice Data Center. doi: https://doi.org/10.7265/N59P2ZTG. [2017-07-04].
* Peng, G., W. Meier, D. Scott, and M. Savoie. 2013. A long-term and reproducible passive microwave sea ice concentration data record for climate studies and monitoring, Earth Syst. Sci. Data. 5. 311-318. https://doi.org/10.5194/essd-5-311-2013

# Changelog

## 2018-08-23
* Lower and upper boundaries in vector data set to 0.5% and 99.5% respectively
* Minimum and maximum extent shapefiles are not included anymore since the information is already provided in the isoband files
* Attribute name for ice frequency values in shapefile attribute table is changed from "value" to "ice_freq"

## 2018-07-02
This dataset update includes a major change where the new type of source data is used: (Sea Ice Concentration Climate Data Record, G02202-v3)[http://nsidc.org/data/G02202/versions/3].
Major changes:
* Transition to NSIDC G02202-v3 Sea Ice Concentration Climate Data Record dataset.
* Filling polar pole: we assume that the polar hole areas always contain sea ice concentrations above 15%. Therefore we set the values to 100%
* In contrast with the previous processing scheme we do not fill the missing days with the values from the preceding day in 1987. However the weight of each observation is maintained and  the final occurence frequency values are not affected.
