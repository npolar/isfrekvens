#!/usr/bin/env python

import sys
import xarray as xr
import numpy as np
import os, shutil
import rasterio
import multiprocessing as mp
from itertools import product
from functools import partial
import fnmatch

from matplotlib import pyplot as plt


def compute_isfrekvens(path, month, filename_pattern=None):
    dst = xr.open_mfdataset(path, autoclose=True)
    qa = dst.variables['qa_of_seaice_conc_cdr']
    data = dst.variables['seaice_conc_cdr']

    # look into the dataset description for mask codes
    lmask = _mask(data, mask_value=2.54)
    hmask = _mask(data, mask_value=2.51)
    cmask = _mask(data, mask_value=2.53)
    mmask = np.where(qa[0,:,:]==15, False, True)
    missing_days = np.sum(np.array([np.all(x) for x in data[:]]))

    # compute number of valid days
    # as a number of all occurences of 0 or positive numbers
    days = np.where(data[:]>=0,1,0).sum(axis=0)

    # compute frequency and apply scale of 100%
    mean_dst = np.where(data[:]>=0.15, 1, 0).sum(axis=0)/days*100

    # apply various masks from the original data source
    masked_dst = np.where(lmask==False, -2, mean_dst)
    masked_dst = np.where(hmask==False, 100, masked_dst)
    masked_dst = np.where(cmask==False, -3, masked_dst)
    # TODO remove -4 tag
    masked_dst = np.where(mmask==False, -4, masked_dst)
    return masked_dst


def _mask(src, mask_value=2.54):
    mask = np.prod(np.where(src[:]==mask_value, False, True), axis=0)
    return mask


def find_files(root_path, year_list, month):
    """
    Walk directories and apply file name filter
    for years and month selection
    """
    matches = []
    years = ["seaice_conc_daily_nh_???_{:4d}{:02d}??_*.nc".format(year, month) for year in year_list]
    for root, dirs, files in os.walk(root_path):
        for year in years:
            for filename in fnmatch.filter(files, year):
                matches.append(os.path.join(root, filename))
    return matches


def make_isfrekvens_product(m, data_dir=None, years=None):

    year_start = years[0]
    year_end = years[-1]
    year_list = range(year_start, year_end)
    file_list = find_files(data_dir, year_list, m)

    month = "{:02d}".format(m)
    
    mdict = {
             "month": m,
             "data_dir": data_dir, 
             'year_start': year_start, 
             'year_end': year_end
             }
    
    print("Computing ice frequency for month: {}".format(month))

    xdst = compute_isfrekvens(file_list, m)
    template_file = 'test_data/sea_ice_template.tif'
    output_file = './output/seaice_frequency_{year_start:4d}-{year_end:4d}_{month:02d}.tif'.format(**mdict)
    with rasterio.open(template_file) as src:
        profile = src.profile
        profile['dtype'] = rasterio.float64
    with rasterio.open(output_file, mode='w', **profile) as dst:
        dst.write(xdst, 1)
    print("Saved ice frequency for month: {}".format(month))
    return None


def main():
    year_start=int(sys.argv[1])
    year_end=int(sys.argv[2])
    data_dirs = [sys.argv[3],]
    months = range(1,13)
    pool = mp.Pool(processes=4)

    pool.map(partial(
              make_isfrekvens_product,
              data_dir=data_dirs,
              years=range(year_start, year_end+1)
             ),
             months
            )

if __name__ == "__main__":
    main()
