#!/usr/bin/env python
"""
Usage:
  contours_isfrekvens.py --levels=LEVELS INPUT_FILE OUTPUT_FILE
  contours_isfrekvens.py (-h | --help)
  contours_isfrekvens.py --version

Options:
  -l LEVELS --levels=LEVELS
  -h --help     Show this screen.
  -v --version     Show version.

"""

from docopt import docopt

import os

import geopandas as gp
import numpy as np
import pandas as pd
import rasterio as rst
from shapely.geometry import linestring
from contours.core import shapely_formatter as shapely_fmt
from contours.quad import QuadContourGenerator
from skimage.filters import gaussian
import scipy



def get_xy_coords(dst):
    """
    Extract X and Y coordinates from rasterio dataset
    Parameters
    ----------
    |    dst : rasterio.DatasetReader
    Returns
    -------
        (x, y) : tuple with coordinate arrays
    """
    try:
        dx = dst.res[0]
        dy = dst.res[1]
    except:
        raise

    x = np.linspace(dst.bounds.left + dx / 2., dst.bounds.right - dx / 2., dst.width)
    y = np.linspace(dst.bounds.bottom + dy / 2., dst.bounds.top - dy / 2., dst.height)

    return x, y


def _extract_polygon(src_data, dx, dy, minval, maxval):
    """
    """
    src_gauss = gaussian(np.where((src_data > minval)*(src_data<=maxval), 1, 0),
                         sigma=1.5,
                         preserve_range=True)

    c = QuadContourGenerator.from_rectilinear(dx,
                                              dy,
                                              np.flipud(src_gauss),
                                              shapely_fmt)

    contour = c.filled_contour(min=0.5, max=3.0)
    contour = c.contour(0.5)

    results = ({'properties': {VARNAME: minval}, 'geometry': linestring.asLineString(s)}
               for i, s in enumerate(contour))
    geoms = list(results)
    return geoms


def extract_polygons(src_data, output_path, levels, dx, dy, transform=None, maxval=101, src=None):
    """
    """

    gpd_prev = None
    gpd_diff = None

    if os.path.exists(output_path):
            os.remove(output_path)

    _ = []

    for level in levels:
        print("LEVEL: {}".format(level))
        geoms = _extract_polygon(src_data,
                                 dx,
                                 dy,
                                 level,
                                 maxval
                                 )

        gpd_current = gpd_orig = gp.GeoDataFrame.from_features(geoms)
        gpd_current.crs = src.crs
        _.append(gpd_current)

    gpd_final = pd.concat(_, ignore_index=True).pipe(gp.GeoDataFrame)
    gpd_final.crs = _[0].crs
    DRIVER = "ESRI Shapefile"
    gpd_final.to_file(output_path, driver=DRIVER)


def main():
    arguments = docopt(__doc__, version='0.1')
    input_file = arguments['INPUT_FILE']
    output_path = arguments['OUTPUT_FILE']

    src = rst.open(input_file)
    dx, dy = get_xy_coords(src)
    src_data = src.read(1)

    # Use reverse list with (required) level boundaries
    levels = [ float(x) for x in arguments['--levels'].split(',')][::-1]

    extract_polygons(src_data,
                     output_path,
                     levels,
                     dx, dy,
                     transform=src.transform,
                     src=src)


if __name__ == "__main__":
    main()

