#!/usr/bin/env python3

"""
Usage:
  upsample_seaice_frequency.py --landmask=LANDMASK_FILE --levels=LEVELS --feature_type=FEATURE_TYPE INPUT_FILE OUTPUT_FILE
  upsample_seaice_frequency.py (-h | --help)
  upsample_seaice_frequency.py --version

Options:
  -h --help     Show this screen.
  -v --version     Show version.

"""

import geopandas as gp
import numpy as np
import pandas as pd
import rasterio as rst
from contours.core import shapely_formatter as shapely_fmt
from contours.quad import QuadContourGenerator
from docopt import docopt
from pyresample import kd_tree, geometry, utils
from rasterio.features import shapes
from shapely.geometry import linestring
from skimage.filters import gaussian

VARNAME='ice_freq'
CRS = 'epsg:3411'

def upsample_icefreq(data, mask, smooth=True):
    """
    Upsample ice frequency fields but not land.
    The land mask is to be applied separately.

    Use pyresample/pykdtree resampling to explicitly provide
    the interpolation distance

    """
    area_25k = utils.load_area('./areas.cfg', 'nsidc_25k')
    area_05k = utils.load_area('./areas.cfg', 'nsidc_05k')
    area_10k = utils.load_area('./areas.cfg', 'nsidc_10k')

    # cache coordinate arrays
    lon_1, lat_1 = area_05k.get_lonlats()
    # lon_1, lat_1 = area_10k.get_lonlats()
    lon_0, lat_0 = area_25k.get_lonlats()

    # define grid and swath (irregular grid)
    swath_def = geometry.SwathDefinition(lon_0[mask], lat_0[mask])
    grid_def  = geometry.GridDefinition(lats=lat_1, lons=lon_1)

    # resample using gaussian smoothing
    res = kd_tree.resample_gauss(swath_def,
            data[mask],
            grid_def,
            radius_of_influence=75000,
            sigmas=25000)

    if smooth:
        res = gaussian(res,
                sigma=2.5,
                preserve_range=True)

    return res


def _get_xy_coords(dst):
    """
    Extract X and Y coordinates from rasterio dataset
    Parameters
    ----------
    |    dst : rasterio.DatasetReader
    Returns
    -------
        (x, y) : tuple with coordinate arrays
    """
    try:
        dx = dst.res[0]
        dy = dst.res[1]
    except:
        raise

    x = np.linspace(dst.bounds.left + dx / 2., dst.bounds.right - dx / 2., dst.width)
    y = np.linspace(dst.bounds.bottom + dy / 2., dst.bounds.top - dy / 2., dst.height)

    return x, y


def _extract_polygon(src_data, dx, dy, minval, maxval):
    """"""
    src_gauss = gaussian(np.where((src_data > minval)*(src_data<=maxval), 1, 0),
                         sigma=1.2,
                         preserve_range=True)

    c = QuadContourGenerator.from_rectilinear(dx,
                                              dy,
                                              np.flipud(src_gauss),
                                              shapely_fmt)

    contour = c.filled_contour(min=0.5, max=3.0)

    results = ({'properties': {VARNAME: minval}, 'geometry': s}
               for i, s in enumerate(contour))

    geoms = list(results)
    geoframe = gp.GeoDataFrame.from_features(geoms)
    geoframe.crs = CRS
    return geoframe


def _extract_contour(src_data, dx, dy, minval, maxval):
    """
    Same as extract polygons but only a contour line instead of a filled polygon
    """
    src_gauss = gaussian(np.where((src_data > minval) * (src_data<=maxval), 1, 0),
                         sigma=1.2,
                         preserve_range=True)

    c = QuadContourGenerator.from_rectilinear(dx,
                                              dy,
                                              np.flipud(src_gauss),
                                              shapely_fmt)

    contour = c.contour(0.5)

    results = ({'properties': {VARNAME: minval}, 'geometry': linestring.asLineString(s)}
               for i, s in enumerate(contour))
    geoframe = gp.GeoDataFrame.from_features(list(results))
    geoframe.crs = CRS
    return geoframe


def difference_line_poly(shp, clip_obj):
    """
    from https://www.earthdatascience.org/courses/earth-analytics-python/spatial-data-vector-shapefiles/clip-vector-data-in-python-geopandas-shapely/
    """
    # Create a single polygon object for clipping
    poly = clip_obj.geometry.unary_union
    spatial_index = shp.sindex

    # Create a box for the initial intersection
    bbox = poly.bounds
    # Get a list of id's for each road line that overlaps the bounding box and subset the data to just those lines
    sidx = list(spatial_index.intersection(bbox))
    shp_sub = shp.iloc[sidx]

    # Clip the data - with these data
    clipped = shp_sub.copy()
    clipped['geometry'] = shp_sub.difference(poly)

    # Return the clipped layer with no null geometry values
    return(clipped[clipped.geometry.notnull()])


def extract_features(src_data, levels, dx, dy, maxval=101, crs=None, feature_type=None) -> gp.GeoDataFrame:
    """
    Extract either polygon or contours from the numpy array
    """
    gpd_prev = None
    gpd_diff = None

    _ = []

    if feature_type == 'polygon':
        polygons = [_extract_polygon(src_data, dx, dy, level, maxval) for level in levels]
        i = 0; li = len(levels) - 1

        while i < li:
            _.append(gp.overlay(polygons[i], polygons[i + 1], how='difference'))
            i += 1
        _.append(polygons[-1])

    elif feature_type == "contour":
        _ = [_extract_contour(src_data, dx, dy, level, maxval) for level in levels]
    else:
        raise TypeError("Unknown feature type {}".format(feature_type))

    gpd = pd.concat(_, ignore_index=True).pipe(gp.GeoDataFrame)
    gpd.crs = crs

    return gpd


def extract_landmask(filepath):
    with rst.open(filepath) as src:
        image = src.read(1)  # first band
        image = np.where(image<0,1,0).astype(np.int16)
        mask = image>=0
        results = ({'properties': {'ice_freq': v}, 'geometry': s }
            for i, (s,v ) in enumerate(
                shapes(image, transform=src.affine)))
    landmask = gp.GeoDataFrame.from_features(list(results))
    landmask = landmask[landmask[VARNAME]==1]

    return landmask


def main():
    arguments = docopt(__doc__, version='0.1')

    input_file = arguments['INPUT_FILE']
    output_file = arguments['OUTPUT_FILE']
    lmsk = arguments['--landmask']
    feature = arguments['--feature_type']

    levels = [float(x) for x in arguments['--levels'].split(',')]

    # Upsample data to 5k scale
    with rst.open(input_file, 'r') as dst:
        data = dst.read(1)
        data = upsample_icefreq(data, data >= -0.01)

    area = utils.load_area('./areas.cfg', 'nsidc_05k')
    dx = area.projection_x_coords
    dy = area.projection_y_coords

    # extract polygons
    ice_features = extract_features(data,
                                levels,
                                dx,
                                np.flipud(dy),
                                crs=CRS,
                                feature_type=feature)

    landmask = gp.GeoDataFrame().from_file(lmsk)

    if feature=="polygon":
        ice_features_diff = gp.overlay(ice_features, landmask, how='difference')

    else:
        print("Applying landmask to contour lines")
        ice_features_diff = difference_line_poly(ice_features, landmask)

    # Almost there
    ice_features_diff.to_file(output_file)


if __name__=="__main__":
    main()
