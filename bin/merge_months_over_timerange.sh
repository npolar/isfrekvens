#!/bin/bash
DATADIR=$1
OUTPUTDIR=$DATADIR/merged_seaice_conc

if [ ! -d "$OUTPUTDIR" ]; then
	mkdir -p $OUTPUTDIR
fi

echo "++++"

if [ -d "$1" ]; then
	for i in {2..12}; do
		MONTH=$(printf %02d $i)
		docker run --rm -v $DATADIR:/tmp cdo_image cdo -s -O mergetime $(docker run --rm -v $DATADIR:/tmp cdo_image find /tmp -type f -name "seaice_conc_*_???_????${MONTH}??_v03r01.nc" | sort -g) /tmp/merged-${MONTH}.nc
		echo $(date) ": merging NetCDF files for month: $MONTH"
	done
else
	exit 1
fi
