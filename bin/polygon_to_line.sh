#!/usr/bin/zsh
#-----
# Convert polygons into exterior ring line for extent display
#-----
#
INFILE=`basename $1`
OUTFILE=$2
OUTDIR=$(dirname "$OUTFILE")
if [ ! -d "$OUTDIR" ]; then
	mkdir "$OUTDIR"
fi
ogr2ogr $OUTFILE $1 -dialect sqlite -f "ESRI Shapefile" -sql "select ST_ExteriorRing(geometry) as geometry from '$INFILE'"
