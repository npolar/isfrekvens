#!/usr/bin/zsh
set -e
#
# Mount directory with the source data
# e.g. sshfs <src> <dst>
# Edit isfrekvens.py and indicate where the data folder is mounted to

STARTYEAR=1991
ENDYEAR=2020
INPUTDIR=/data
OUTDIR=$INPUTDIR/output
#
#
#
if [ ! -d "$OUTDIR" ]; then
         mkdir "$OUTDIR"
	 echo Created output directory "$OUTDIR"
else
	echo "Directory $OUTDIR exists"
fi
#
# Run the ice frequency calculation
#./bin/compute-ice-frequency.py ${STARTYEAR} ${ENDYEAR} ${INPUTDIR}
# CLIPMASK=res/clipmask-north-125k-buf.shp
CLIPMASK=res/landmask_buf-2500.shp

# Generate isobands based on ice frequency data
parallel -P 4 bin/extract_features.py --levels='0.5,10,20,30,40,50,60,70,80,90,99.5' \
	--feature_type=polygon \
	--landmask=$CLIPMASK \
	$OUTDIR/seaice_frequency_${STARTYEAR}-${ENDYEAR}_{}.tif \
	$OUTDIR/seaice_frequency_${STARTYEAR}-${ENDYEAR}_isobands_{} \
   	::: 01 02 03 04 05 06 07 08 09 10 11 12

parallel -P 4 bin/extract_features.py --levels='0.5,10,20,30,40,50,60,70,80,90,99.5' \
	--feature_type=contour \
	--landmask=$CLIPMASK \
	$OUTDIR/seaice_frequency_${STARTYEAR}-${ENDYEAR}_{}.tif \
	$OUTDIR/seaice_frequency_${STARTYEAR}-${ENDYEAR}_isolines_{} \
   	::: 01 02 03 04 05 06 07 08 09 10 11 12


cd ${OUTDIR}

# Collect vector data in the respective folders
ISOLINES_DIR=seaice_frequency_${STARTYEAR}-${ENDYEAR}_isolines
ISOBANDS_DIR=seaice_frequency_${STARTYEAR}-${ENDYEAR}_isobands

if [ -d $ISOLINES_DIR ]; then
	rm -R $ISOLINES_DIR
fi

if [ -d $ISOBANDS_DIR ]; then
	rm -r $ISOBANDS_DIR
fi
mkdir $ISOLINES_DIR
mkdir $ISOBANDS_DIR

cp seaice_frequency_${STARTYEAR}-${ENDYEAR}_isobands_??/* seaice_frequency_${STARTYEAR}-${ENDYEAR}_isobands
cp seaice_frequency_${STARTYEAR}-${ENDYEAR}_isolines_??/* seaice_frequency_${STARTYEAR}-${ENDYEAR}_isolines

zip -r seaice_frequency_${STARTYEAR}-${ENDYEAR}.zip \
   		seaice_frequency_${STARTYEAR}-${ENDYEAR}_isolines/* \
   		seaice_frequency_${STARTYEAR}-${ENDYEAR}_isobands/* \
		seaice_frequency_${STARTYEAR}-${ENDYEAR}_??.tif
cd -
