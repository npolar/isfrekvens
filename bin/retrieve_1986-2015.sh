#!/bin/bash
set -e
PROJECT_ROOT_DIR=$1

if [ -z "$1" ]; then
	echo "Variable '\$1' is empty; usage is ./script <PROJECT_ROOT_DIR>"
	exit 1
fi

FTP=ftp://sidads.colorado.edu/pub/DATASETS/NOAA/G02202_v2/north/daily
DATADIR=${PROJECT_ROOT_DIR}/data
for YEAR in {2006..2015}; do
	DNLDDIR=$DATADIR/$YEAR
	mkdir -p $DNLDDIR
	cd $DNLDDIR
	wget $FTP/$YEAR/*.nc -nc -P $DNLDDIR
done

