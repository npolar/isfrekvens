#!/usr/bin/env python

import rasterio as rst
import numpy as np
import sys

ifile = sys.argv[1]

with rst.open(ifile, 'r+') as dst:
    data = dst.read(1)
    data = np.where(data==-20000, -2, data)
    data = np.where(data==-30000, -3, data)
    data = np.where(data==-40000, -4, data)
    dst.write(data, 1)
