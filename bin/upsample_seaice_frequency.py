#!/usr/bin/env python

"""
Usage:
  upsample_seaice_frequency.py --xsize=XSIZE --ysize=YSIZE INPUT_FILE OUTPUT_FILE
  upsample_seaice_frequency.py (-h | --help)
  upsample_seaice_frequency.py --version

Options:
  -h --help     Show this screen.
  -v --version     Show version.

"""

from docopt import docopt
import os
import rasterio as rst
import numpy as np

from pyresample import kd_tree, geometry, utils
from skimage.filters import gaussian
from rasterio.warp import reproject, calculate_default_transform, Resampling, Affine

def upsample_icefreq(data, mask, smooth=True):
    """
    Upsample ice frequency fields but not land.
    The land mask is to be applied separately.

    Use pyresample/pykdtree resampling to explicitly provide
    the interpolation distance

    """
    area_25k = utils.load_area('./areas.cfg', 'nsidc_25k')
    area_05k = utils.load_area('./areas.cfg', 'nsidc_05k')

    # cache coordinate arrays
    lon_1, lat_1 = area_05k.get_lonlats()
    lon_0, lat_0 = area_25k.get_lonlats()

    # define grid and swath (irregular grid)
    swath_def = geometry.SwathDefinition(lon_0[mask], lat_0[mask])
    grid_def  = geometry.GridDefinition(lats=lat_1, lons=lon_1)

    # resample using gaussian smoothing
    res = kd_tree.resample_gauss(swath_def,
            data[mask],
            grid_def,
            radius_of_influence=75000,
            sigmas=25000)

    if smooth:
        res = gaussian(res,
                sigma=2.5,
                preserve_range=True)

    return res


def _get_xy_coords(dst):
    """
    Extract X and Y coordinates from rasterio dataset
    Parameters
    ----------
    |    dst : rasterio.DatasetReader
    Returns
    -------
        (x, y) : tuple with coordinate arrays
    """
    try:
        dx = dst.res[0]
        dy = dst.res[1]
    except:
        raise

    x = np.linspace(dst.bounds.left + dx / 2., dst.bounds.right - dx / 2., dst.width)
    y = np.linspace(dst.bounds.bottom + dy / 2., dst.bounds.top - dy / 2., dst.height)

    return x, y


def _extract_polygon(src_data, dx, dy, minval, maxval):
    """
    """
    src_gauss = gaussian(np.where((src_data > minval)*(src_data<=maxval), 1, 0),
                         sigma=1.5,
                         preserve_range=True)

    c = QuadContourGenerator.from_rectilinear(dx,
                                              dy,
                                              np.flipud(src_gauss),
                                              shapely_fmt)

    contour = c.filled_contour(min=0.5, max=3.0)

    results = ({'properties': {VARNAME: minval}, 'geometry': s}
               for i, s in enumerate(contour))
    geoms = list(results)
    return geoms


def _extract_contour(src_data, dx, dy, minval, maxval):
    """
    Same as extract polygons but only a contour line instead of a filled polygon
    """
    src_gauss = gaussian(np.where((src_data > minval) * (src_data<=maxval), 1, 0),
                         sigma=1.5,
                         preserve_range=True)

    c = QuadContourGenerator.from_rectilinear(dx,
                                              dy,
                                              np.flipud(src_gauss),
                                              shapely_fmt)

    contour = c.contour(0.5)

    results = ({'properties': {VARNAME: minval}, 'geometry': linestring.asLineString(s)}
               for i, s in enumerate(contour))
    geoms = list(results)
    return geoms


def extract_features(src_data, output_path, levels, dx, dy, maxval=101, crs=None, feature_type=None):
    """
    """
    gpd_prev = None
    gpd_diff = None

    if os.path.exists(output_path):
            os.remove(output_path)

    _ = []

    for level in levels:
        if feature_type == "polygon":
            geoms = _extract_polygon(src_data,
                                 dx,
                                 dy,
                                 level,
                                 maxval)
        elif feature_type == "contour":
            geoms = _extract_contour(src_data,
                                     dx,
                                     dy,
                                     level,
                                     maxval)
        else:
            raise TypeError("Unknown feature type {}".format(feature_type))

        gpd_current = gpd_orig = gp.GeoDataFrame.from_features(geoms)
        gpd_current.crs = crs
        _.append(gpd_current)

    gpd_final = pd.concat(_, ignore_index=True).pipe(gp.GeoDataFrame)
    gpd_final.crs = crs

    return gpd_final


def main():
    arguments = docopt(__doc__, version='0.1')

    input_file = arguments['INPUT_FILE']
    output_path = arguments['OUTPUT_FILE']

    # resample input file to increase the resolution according
    # to provided input XSIZE and YSIZE
    XSIZE = int(arguments['--xsize'])
    YSIZE = int(arguments['--ysize'])

    with rst.open(input_file) as src:
        dst_crs = src.crs
        data = src.read(1)
        src_profile = src.profile
        srt = src_transform = src.transform

        xres = (src.bounds.right - src.bounds.left) / XSIZE
        yres = (src.bounds.top - src.bounds.bottom) / YSIZE

        dst_transform = [srt[0], xres, srt[2], srt[3], srt[4], -yres]

        kwargs = src.meta
        kwargs.update({
            'transform' : dst_transform,
            'width': XSIZE,
            'height': YSIZE,
            'affine': Affine.from_gdal(dst_transform[0],
                dst_transform[1],
                dst_transform[2],
                dst_transform[3],
                dst_transform[4],
                dst_transform[5])
            })

    # Create output array for resampling results
    # and define Affine transform for the new
    # GeoTIFF file
    dst = np.zeros((YSIZE, XSIZE), dtype=np.float64)

    with rst.open(output_path, mode='w', **kwargs) as ofile:
        dst = upsample_icefreq(data, data>=0)
        ofile.write(dst, 1)

if __name__=="__main__":
    main()
