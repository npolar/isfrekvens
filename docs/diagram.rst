Processing diagram
------------------

.. mermaid::
   
   graph TD 
      A[NSIDC daily sea ice concentration files] --> |generate monthly files| B[Ice frequency monthly files]
      B --> |upsample frequency values only, no land| C[Upsampled sea ice frequency geotiffs]
      C -->|vectorize raster product| D[Ice frequency contours]
      D --> E[Ice frequency polygons, aka isobands ]
      E --> F[Polygons with no data features]
      B --> |extract landmask separately| F
      G[Auxiallary highres land mask] --> | clip isobands with highres landmask| F
      F --> |store files| H[Dataset deliverable package ]
      B --> |store  files| H
      D --> |store files| H
