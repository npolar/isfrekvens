.. Sea Ice Frequency documentation master file, created by
   sphinx-quickstart on Wed Jul  4 23:32:37 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Sea Ice Frequency Dataset documentation!
===================================================

.. mdinclude:: ../README.md
.. include:: ../docs/diagram.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
