#!/bin/bash

FTP_HOST=ftp://sidads.colorado.edu

for YEAR in {1989..2019}; do
    lftp -e "find DATASETS/NOAA/G02202_V3/north/daily/${YEAR}; bye" $FTP_HOST \
        | grep .nc \
        | sed "s|^|${FTP_HOST}/|" \
        >> file_urls.txt

done

cat file_urls.txt | xargs -n 1 -P 10 wget -q